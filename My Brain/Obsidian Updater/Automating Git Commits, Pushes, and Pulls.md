[Git](https://git-scm.com/downloads) has a command line, Git Bash. Git Bash can be run from a script file. My intention is to run a Git Bash script periodically using Task Scheduler.

Committing and pushing (to add your changes then update them) could be done at the same time, periodically.

Pulling (to update your local repository and your workspace simultaneously) could be done when required or when Obsidian is opened.
## Commit
	git commit -m "Automated <HOST> Commit."]

Committing with a message to show which host computer the automated commit came from. The possible know hosts are as follows:

- PANOPTICON (Home Laptop)
- LIGHTER (Phone)
- HILLTOP (College Laptop)
## Push
	git push <URL> main

Pushing to the repository address ([this](https://github.com/Heftygolfish31/ObsidianUpdater)) with the target branch as main.
## Pull
	git pull

Provided the script is ran from the active directory, git pull needs no more options.
## Constructing the Script

All in all, the script looks like this.

```
git pull

git add .

git commit -m "Automated <HOST> Commit"

git push <URL> main

exit
```
	The script in essence.

This shell script runs on start-up (as does Obsidian) and then runs again, every 10 mins for 12 hours but it won't wake it on sleep. These runtimes will not run while another instance is scheduled. This is what Task Scheduler thinks will happens.
## This works!

It works. I can see how this could have flaws but I just have to wait 5 mins to shut the computer if I want to use Obsidian on a different device.

## Log

- *10/08/2023* - PANOPTICON and HILLTOP task scheduler run from start and commit changes every 5 mins. LIGHTER uses [MGit](https://play.google.com/store/apps/details?id=com.manichord.mgit&hl=en&gl=US&pli=1) to pull to android app but changes cannot be made from phone at this time (MGit doesn't recognise the repo as public because there is no option to log in).

#obsidian_updater #description #git