**YYYY-MM-DD Study Timetable**

- [ ] 0000 - 0015: Class 1 Write-up.
- [ ] 0015 - 0020: Break.
- [ ] 0020 - 0035: Class 2 Write-up.
- [ ] 0035 - 0040: Break.
- [ ] 0040 - 0055: Admin and Other.

**Total Runtime:**
**Total Study Time:**
**Total Break Time:**