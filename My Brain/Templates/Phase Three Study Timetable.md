**YYYY-MM-DD Study Timetable**

- [ ] 0000 - 0010: Class 1 Write-up.
- [ ] 0010 - 0015: Break.
- [ ] 0015 - 0025: Class 2 Write-up.
- [ ] 0025 - 0030: Break.
- [ ] 0030 - 0050: Revise Unit.
- [ ] 0050 - 0055: Break.
- [ ] 0055 - 0115: Revise Unit.
- [ ] 0115 - 0120: Break.
- [ ] 0120 - 0135: Admin and Other.

**Total Runtime:**
**Total Study Time:**
**Total Break Time:**