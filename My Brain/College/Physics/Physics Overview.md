This subject has been allocated **41 hours** and is projected to take as long.

Time spent on this subject to date: **0 hours**.
Unit Completion: **0/12**.

### Contents

- Units
	- [[Physics Unit 3 Overview|Unit 3]]
	- [[Physics Unit 4 Overview|Unit 4]]
	- [[Physics Unit 5 Overview|Unit 5]]

### Links
- [WJEC Page for Physics A Level](https://www.wjec.co.uk/qualifications/physics-as-a-level/#tab_keydocuments)
- [WJEC Physics A Level Specification](https://www.wjec.co.uk/umbraco/surface/blobstorage/download?nodeId=2928) ([[wjec-physics-spec.pdf|downloaded]])

#college #description #physics 