## Contents:

- Units
	1. [[College/Computer Science/Unit 4/Hardware and Communication|Hardware and Communication]]
	2. [[College/Computer Science/Unit 4/Data Transmission|Data Transmission]]
	3. [[College/Computer Science/Unit 4/Data Representation and Data Types|Data Representation and Data Types]]
	4. [[College/Computer Science/Unit 4/Organisation and Structure of Data|Organisation and Structure of Data]]
	5. [[College/Computer Science/Unit 4/Databases and Distributed Systems|Databases and Distributed Systems]]
	6. [[College/Computer Science/Unit 4/The Operating System|The Operating System]]
	7. [[College/Computer Science/Unit 4/The Need for Different Types of Software Systems and their Attributes|The Need for Different Types of Software Systems and their Attributes]]
	8. [[College/Computer Science/Unit 4/Data Security and Integrity Processes|Data Security and Integrity Processes]]

#college #description #computer_science 