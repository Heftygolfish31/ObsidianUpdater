## Contents:

- Units
	1. [[College/Computer Science/Unit 3/Data Structures|Data Structures]]
	2. [[College/Computer Science/Unit 3/Logical Operations|Logical Operations]]
	3. [[College/Computer Science/Unit 3/Algorithms and Programs|Algorithms and Programs]]
	4. [[College/Computer Science/Unit 3/Principles of Programming|Principles of Programming]]
	5. [[College/Computer Science/Unit 3/Systems Analysis|Systems Analysis]]
	6. [[College/Computer Science/Unit 3/System Design|System Design]]
	7. [[College/Computer Science/Unit 3/Software Engineering|Software Engineering]]
	8. [[College/Computer Science/Unit 3/Program Construction|Program Construction]]
	9. [[College/Computer Science/Unit 3/Economic, Moral, Legal, Ethical, Cultural Issues relating to Computer Science|Economic, Moral, Legal, Ethical, Cultural Issues relating to Computer Science]]


#college #description #computer_science 