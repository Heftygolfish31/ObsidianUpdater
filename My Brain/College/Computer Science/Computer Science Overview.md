This subject has been allocated **41 hours** but is projected to take less.

Time spent on this subject to date: **0 hours**.
Unit Completion: **0/??**.

### Contents
- Units
	- [[Computer Science Unit 3 Overview|Unit 3]]
	- [[Computer Science Unit 4 Overview|Unit 4]]
	- [[Computer Science Unit 5 Overview|Unit 5]]

### Links
- [WJEC Page for Computer Science A Level](https://www.wjec.co.uk/qualifications/computer-science-as-a-level/#tab_keydocuments)
- [WJEC Computer Science A Level Specification](https://www.wjec.co.uk/umbraco/surface/blobstorage/download?nodeId=4408)([[wjec-computer-science-spec.pdf|downloaded]])

#college #description #computer_science 