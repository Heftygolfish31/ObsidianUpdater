"College is a hell of a ride and usually hell." -me, 2023
## Subjects

How I plan to distribute my time:
- [[Time Management]]

The subjects I am taking this year are:
- [[Maths Overview|Maths]]
- [[Further Maths Overview|Further Maths]]
- [[Physics Overview|Physics]]
- [[Computer Science Overview|Computer Science]]

## University

University is a time for learning and world building.
### Courses

Some course titles I'm interested in:
- Computer Science BSc / MComp
- Computer Science and Cyber Security BSc / MComp

### University

Some universities I'm interested in:
- [[Southampton]]
- [[Imperial London]]
- [[Oxford]]
- [[Cambridge]]

#college #description #decisions #university

#maths #further_maths #physics #computer_science 