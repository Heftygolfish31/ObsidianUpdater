I need to manage my time better this year. I have four subjects and I should aim to roughly spend a quarter of my study time on each.

A version of the templates show is available here:
- [[Phase One Study Timetable]]
- [[Phase Two Study Timetable]]
- [[Phase Three Study Timetable]]
- [[Phase Four Study Timetable]]
## What we know

Facts:
1. There are 34 weeks (roughly) of the A-Level academic year.
2. Exam season lasts 2 weeks.
3. Exam preparation is necessary if success is to be expected or even hoped for.

Takeaways:
1. There are 32 weeks that can be used for studying.
2. That leaves 8 weeks per subject.
## Goals

- Pass my exams.
- Exercise equity when preparing for exams.
- Ramp up my preparation in the time leading to the exam season.

## Plan of Attack

The plan of attack for most efficiently handling my time is to be fourfold. I could spend 8 weeks on each intensity but that seems ill-advised when faced with the final stretch seeing as that would require furious work for 8 whole weeks.

**Homework is to be completed in D10 or after class write-ups.**

### A Note on Course Work

Note-taking should still be conducted when it comes to course work but all work should be done in college. D10 will be the place for Computer Science and Physics Course work. 

### Phase One: 14 weeks / 70 days /  64 hours

The longest period but can be fast-tracked if so whished. I expect that two weeks worth of this will be taken up with reintegration, the write-up of classes, and the building of habit.

I expect to spend 15 mins per class studied that day with another 15 mins for admin, building templates and practicing.

**With an expected runtime of 55 mins, work should be started no later than 2115.**

For example:

**YYYY-MM-DD Study Timetable**

- [ ] 0000 - 0015: Class 1 Write-up.
- [ ] 0015 - 0020: Break.
- [ ] 0020 - 0035: Class 2 Write-up.
- [ ] 0035 - 0040: Break.
- [ ] 0040 - 0055: Admin and Other.

**Total Runtime:**
**Total Study Time:**
**Total Break Time:**
### Phase Two: 8 weeks / 40 days / 53 hours

By this time, I should be pretty comfortable with the hour-ish of work per night. Maybe by this point the class write-ups are getting easier and simpler. Now they will be allocated a minimum of 10 mins. Next we will revise and write notes for one unit of any choice choice to be logged in this vault.

**With an expected runtime of 1 hour and 20 mins, work should be started no later than 2050.**

For example:

**YYYY-MM-DD Study Timetable**

- [ ] 0000 - 0010: Class 1 Write-up.
- [ ] 0010 - 0015: Break.
- [ ] 0015 - 0025: Class 2 Write-up.
- [ ] 0025 - 0030: Break.
- [ ] 0030 - 0050: Revise Unit.
- [ ] 0050 - 0055: Break.
- [ ] 0055 - 0120: Admin and Other.

**Total Runtime:**
**Total Study Time:**
**Total Break Time:**

### Phase Three: 6 weeks / 30 days / 48 hours

Okay so things are getting sticky and tense so lets try and calm it and stay on top. Much the same of the structure can be seen in the previous phase with another unit revision session.

**With an expected runtime of 1 hour and 35 mins, work should be started no later than 2035.**

For example:

**YYYY-MM-DD Study Timetable**

- [ ] 0000 - 0010: Class 1 Write-up.
- [ ] 0010 - 0015: Break.
- [ ] 0015 - 0025: Class 2 Write-up.
- [ ] 0025 - 0030: Break.
- [ ] 0030 - 0050: Revise Unit.
- [ ] 0050 - 0055: Break.
- [ ] 0055 - 0115: Revise Unit.
- [ ] 0115 - 0120: Break.
- [ ] 0120 - 0135: Admin and Other.

**Total Runtime:**
**Total Study Time:**
**Total Break Time:**
### Phase Four: 4 weeks / 20 days / Final Hours

With little or ill preparation as we enter this phase, shit will hit the fan.

Beloved class write-ups will no longer be of upmost importance as units should have concluded or are being concluded during this time. This is the time for past exam papers. 7 of each to be exact. This does not include homework past papers.

Timetabling here is general as not all papers are made equal. Given that the longest exam lasts 2 hours 30 mins, that is the scale we will measure ourselves by.

**That being said and with an expected maximum runtime of 3 hour and 30 mins, work should be started no later than 1900. Adjust times respectively for all other exams.**

For example:

**YYYY-MM-DD Study Timetable**

- [ ] (EXAM LENGTH): Practice Exam.
- [ ] (20 mins): Break.
- [ ] (20 mins): Exam Review.
- [ ] (5 mins): Break.
- [ ] (15 mins): Admin and Other.

**Total Runtime:**
**Total Study Time:**
**Total Break Time:**

### Reflection

This is a projected **32 weeks** of work during which time I aim to complete **165 hours** of independent study time aside from that done in college. That works out as roughly **41 hours** of each lesson.

#college #description #management

#computer_science #physics #maths #further_maths 