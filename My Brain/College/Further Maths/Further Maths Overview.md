This subject has been allocated **41 hours** but is projected to take more.

Time spent on this subject to date: **0 hours**.
Unit Completion: **0/??**.

### Contents

### Links
- [WJEC Page for Further Maths A Level](https://www.wjec.co.uk/qualifications/mathematics-a-as-level/#tab_keydocuments)
- [WJEC Further Maths A Level Specification](https://www.wjec.co.uk/umbraco/surface/blobstorage/download?nodeId=7994) ([[wjec-further-maths-spec.pdf|downloaded]])
- 