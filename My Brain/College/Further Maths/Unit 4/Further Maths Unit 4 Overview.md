## Contents:

- Units
	1. [[College/Further Maths/Unit 4/Complex Numbers|Complex Numbers]]
	2. [[College/Further Maths/Unit 4/Further Trigonometry|Further Trigonometry]]
	3. [[College/Further Maths/Unit 4/Matrices|Matrices]]
	4. [[College/Further Maths/Unit 4/Further Algebra and Functions|Further Algebra and Functions]]
	5. [[College/Further Maths/Unit 4/Further Calculus|Further Calculus]]
	6. [[College/Further Maths/Unit 4/Polar Coordinates|Polar Coordinates]]
	7. [[College/Further Maths/Unit 4/Hyperbolic Functions|Hyperbolic Functions]]
	8. [[College/Further Maths/Unit 4/Differential Equations|Differential Equations]]

#college #description #further_maths 