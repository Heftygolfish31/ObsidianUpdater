Physics engines are scary things so I intend to break down the larger problems into manageable, smaller problems.

My research starts off as b2d0. B2D is a currently existing package for creating physics engines. JB2D, in particular, is old and has a hard time interacting with Processing 4. Processing is a Java based programming tool used for simple programming art. Processing is also old and rather soft around the edges; that is to say, it is simple to get the hang of but certainly not prepared for more complex algorithms. I will migrate to a tool which is closer to a Java package that I can use with any IDE.

- [[b2d0 Overview]] is an archive of Processing 4 prototypes.

#physics_engine #description