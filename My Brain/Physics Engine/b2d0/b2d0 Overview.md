b2d0 is a library of prototypes built in Processing 4 (so named after B2D). They are included here as a record and a reference, not an active directory.

Accessing them through Obsidian is not possible but they are linked in the files they appear as (e.g. 'b2d1_proxima' holds just that when viewed from a file explorer or IDE).

#processing #b2d0 #archive #physics_engine 