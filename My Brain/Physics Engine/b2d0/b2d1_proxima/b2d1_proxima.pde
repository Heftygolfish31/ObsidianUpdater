Proxima proxima1, proxima2;
ArrayList<Box> boxes = new ArrayList<Box>();

void setup() {

  size(600, 600);
  frameRate(15);
  background(0);
  stroke(255);
  noFill();

  proxima1 = new Proxima(150, new PVector(width/2, height/2), color(255, 255, 255));
}

void draw() {

  boxes.add(new Box(new PVector(random(20, 50), random(20, 50)), new PVector(random(width), random(height)), color(255, 255, 255)));

  proxima1.update();

  for (Box box : boxes) {
    box.update();
  }
}

class Proxima {

  boolean isMouseNear;
  int radius;
  PVector position;
  color colour;

  Proxima (int radius_, PVector position_, color colour_) {
    radius = radius_;
    position = position_;
    colour = colour_;
  }

  //boolean mouseNear() {

  //  if (position.dist(new PVector(mouseX, mouseY)) <= 150) {
  //    stroke(255, 0, 0);
  //    return true;
  //  } else {
  //    stroke(255);
  //    return false;
  //  }

  //}

  void scanBoxes(ArrayList<Box> boxes) {

    for (Box box : boxes) {

      ArrayList<PVector> currentBoxPoints = box.getPoints();

      for (PVector point : currentBoxPoints) {

        if (position.dist(point) <= radius) {
  
          colour = color(255, 0, 0);
          box.setColour(color(255, 0, 0));
          
          break;
        }
        
      }
    }
  }

  void update() {

    scanBoxes(boxes);

    stroke(colour);
    point(position.x, position.y);
    
    circle(position.x, position.y, radius*2);
  }

  int getRadius() {
    return radius;
  }

  void setRadius(int radius_) {
    radius = radius_;
  }

  PVector getPosition() {
    return position;
  }

  void setPosition(PVector position_) {
    position = position_;
  }
  
  color getColour() {
    return colour;
  }

  void setColour(color colour_) {
    colour = colour_;
  }
}

class Box {

  PVector dimensions, position;
  ArrayList<PVector> points = new ArrayList<PVector>();
  color colour;

  Box (PVector dimensions_, PVector position_, color colour_) {

    dimensions = dimensions_;
    position = position_;
    colour = colour_;

    points.add(position);
    points.add(new PVector(position.x + dimensions.x, position.y));
    points.add(new PVector(position.x + dimensions.x, position.y + dimensions.y));
    points.add(new PVector(position.x, position.y + dimensions.y));
  }

  void update() {

    stroke(colour);
    rect(position.x, position.y, dimensions.x, dimensions.y);
  }

  PVector getDimensions() {
    return dimensions;
  }

  void setDimensions(PVector dimensions_) {
    dimensions = dimensions_;
  }

  PVector getPosition() {
    return position;
  }

  void setPosition(PVector position_) {
    position = position_;
  }

  color getColour() {
    return colour;
  }

  void setColour(color colour_) {
    colour = colour_;
  }

  ArrayList<PVector> getPoints() {
    return points;
  }

  void setPoints(ArrayList<PVector> points_) {
    points = points_;
  }
}
