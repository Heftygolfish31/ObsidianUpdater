ArrayList<Box> boxes = new ArrayList<Box>();
float g = 0.0981;

void setup() {

  size(800, 800);
  frameRate(60);
  background(0);
  frameRate(60);
  noFill();
}

void draw() {

  background(0);

  boxes.add(new Box(new PVector(10, 10), new PVector(width/2-5, 100), new PVector(0, -2), new PVector(random(-0.03, 0.03), 0), true));

  for (Box box : boxes) {
    box.update();
  }
}

class Box {

  boolean gravity;
  PVector dimensions, position, velocity, acceleration;

  Box (PVector dimensions_, PVector position_, PVector velocity_, PVector acceleration_, boolean gravity_) {

    dimensions = dimensions_;
    position = position_;
    velocity = velocity_;
    acceleration = acceleration_;
    gravity = gravity_;
  }

  void update() {

    stroke(255);

    if (gravity) {
      velocity.add(new PVector(0, g));
    }

    velocity.add(acceleration);
    position.add(velocity);
    rect(position.x, position.y, dimensions.x, dimensions.y);
  }
}
