ArrayList<Box> boxes = new ArrayList<Box>();
Box box1;
Box box2;

void setup() {
  size(800, 800);
  frameRate(60);
  background(0);
  noFill();

  box1 = new Box(new PVector(2, 2), new PVector(400, 100), new PVector(0, 0), new PVector(0, 0.1));
  box2 = new Box(new PVector(50, 50), new PVector(400-25, 200), new PVector(0, 0), new PVector(0, 0.025));

  boxes.add(box1);
  boxes.add(box2);
}

void draw() {

  background(0);

  for (Box box : boxes) {
    box.update();
  }
}

class Box {

  boolean isColliding;
  PVector dimensions, position, velocity, acceleration;
  //ArrayList<PVector> points = new ArrayList<PVector>();
  //ArrayList<PVector> edges  = new ArrayList<PVector>();
  PVector center;

  Box (PVector dimensions_, PVector position_, PVector velocity_, PVector acceleration_) {

    dimensions = dimensions_;
    position = position_;
    velocity = velocity_;
    acceleration = acceleration_;
  }

  void update() {

    //points.add(position);
    //points.add(new PVector(position.x + dimensions.x, position.y));
    //points.add(new PVector(position.x + dimensions.x, position.y + dimensions.y));
    //points.add(new PVector(position.x, position.y + dimensions.y));

    //edges.add(new PVector(position.x + dimensions.x/2, position.y));
    //edges.add(new PVector(position.x + dimensions.x, position.y + dimensions.y/2));
    //edges.add(new PVector(position.x + dimensions.x/2, position.y + dimensions.y));
    //edges.add(new PVector(position.x, position.y + dimensions.y/2));
    
    center = new PVector(position.x + dimensions.x/2, position.y + dimensions.y/2);

    stroke(255);

    velocity.add(acceleration);

    for (Box box : boxes) {
      if ((box.center != null) && (box.center != center)) {
        line(center.x, center.y, box.center.x, box.center.y);
        
        ////PROOF OF FUCKING CONCEPT BAYBEEEE
        //if (center.dist(box.center) <= dimensions.x) {
        //  velocity = new PVector(0,0);
        //}
      }
    }
    

    position.add(velocity);
    rect(position.x, position.y, dimensions.x, dimensions.y);
  }
}
