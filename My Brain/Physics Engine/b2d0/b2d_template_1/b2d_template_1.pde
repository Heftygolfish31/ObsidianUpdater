void setup() {
  size(800, 800);
  frameRate(60);
  background(0);
  frameRate(60);
  noFill();
}

void draw() {
  
}

class Box {

  boolean gravity;
  PVector dimensions, position, velocity, acceleration;

  Box (PVector dimensions_, PVector position_, PVector velocity_, PVector acceleration_, boolean gravity_) {

    dimensions = dimensions_;
    position = position_;
    velocity = velocity_;
    acceleration = acceleration_;
    gravity = gravity_;
  }

  void update() {

    stroke(255);

    if (gravity) {
      velocity.add(new PVector(0, g));
    }

    velocity.add(acceleration);
    position.add(velocity);
    rect(position.x, position.y, dimensions.x, dimensions.y);
  }
}
