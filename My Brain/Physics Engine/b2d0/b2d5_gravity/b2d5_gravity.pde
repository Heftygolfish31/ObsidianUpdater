float g = 0.0981;
Box box1;

void setup() {
  size(800, 800);
  frameRate(60);
  frameRate(60);
  noFill();
  stroke(255);

  box1 = new Box(20, new PVector(50, 50), new PVector(width/2-25, 100), new PVector(0, 0), new PVector(0, 0), true);
}

void draw() {

  background(0);

  box1.update();
}

class Box {

  boolean gravity;
  float mass;
  PVector dimensions, position, velocity, acceleration;

  Box (float mass_, PVector dimensions_, PVector position_, PVector velocity_, PVector acceleration_, boolean gravity_) {

    mass = mass_;
    dimensions = dimensions_;
    position = position_;
    velocity = velocity_;
    acceleration = acceleration_;
    gravity = gravity_;
  }

  void update() {

    if (gravity) {
      acceleration = new PVector(acceleration.x, mass*g);
    }

    velocity.add(acceleration);
    position.add(velocity);
    rect(position.x, position.y, dimensions.x, dimensions.y);
  }
}
